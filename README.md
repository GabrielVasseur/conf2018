
## How we track all changes to our splunk deployment

Welcome to the page created for this session from Splunk's annual conference .conf2018!

<----- Please go to "Downloads" to get the slides.

A recording of the session will be available from splunk's website soon, we will provide a link here.


Your hosts:

* [Gabriel Vasseur](https://www.linkedin.com/in/gabrielvasseur/)   gabriel.vasseur@uk.thalesgroup.com
* [Olivier Lauret](https://www.linkedin.com/in/olivierlauret/)   olivier@octamis.com


